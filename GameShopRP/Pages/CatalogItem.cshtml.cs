﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GameShopRP.Models;
using GameShopRP.Services.Catalog;
using GameShopRP.Services.Order;
using GameShopRP.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GameShopRP.Pages
{
    public class CatalogItemModel : PageModel
	{
		private readonly ICatalogService _catalogService;
		private readonly IOrderService _orderService;
		private readonly UserManager<ShopUser> _userManager;
		private readonly SignInManager<ShopUser> _signInManager;

		public CatalogItemModel(ICatalogService catalogService, 
			IOrderService orderService, 
			UserManager<ShopUser> userManager,
			SignInManager<ShopUser> signInManager)
		{
			_catalogService = catalogService;
			_orderService = orderService;
			_userManager = userManager;
			_signInManager = signInManager;
		}

		public CatalogItemViewModel ItemModel { get; set; } = new CatalogItemViewModel();

		//public void OnGet()
  //      {
  //      }

		public async Task OnGetLoadItem(int prodId)
		{
			ItemModel = await _catalogService.GetCatalogItemByIdAsync(prodId);
		}

		public async Task<IActionResult> OnPostAddToCart(int prodId)
		{
			if (_signInManager.IsSignedIn(HttpContext.User))
			{
				var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
				await _orderService.AddItemToOrder(userId, prodId);
				//return RedirectToPage("CatalogItem", "LoadItem", new { prodId });
				return Redirect(string.Format("~/CatalogItem/LoadItem/{0}", prodId));
			} else
			{
				return Redirect("~/Identity/Account/Login");
			}
		}
    }
}