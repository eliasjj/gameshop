﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GameShopRP.Data.Repositories
{
	public interface IAsyncRepository<T>
	{
		Task<T> GetByIdAsync(int id);
		Task<List<T>> ListAllAsync();
		Task<List<T>> GetListAsync(Expression<Func<T, bool>> expression);
		Task<List<T>> GetListAsync(Expression<Func<T, bool>> expression, string navigationPropertyPath);
		Task<List<T>> GetListAsync(string navigationPropertyPath);
		Task<List<T>> GetListAsync(Expression<Func<T, object>> expression);
		Task<T> AddAsync(T entity);
		Task UpdateAsync(T entity);
		Task DeleteAsync(T entity);
	}
}
