﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameShopRP.Services.Catalog;
using GameShopRP.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GameShopRP.Pages
{
	public class IndexModel : PageModel
	{
		private readonly ICatalogService _catalogService;

		public IndexModel(ICatalogService catalogService)
		{
			_catalogService = catalogService;
		}
		[BindProperty(SupportsGet = true)]
		public int CurrentPage { get; set; } = 1;
		public int Count { get; set; }
		public int PageSize { get; set; } = 10;

		public int TotalPages => (int)Math.Ceiling(decimal.Divide(Count, PageSize));

		public CatalogViewModel CatalogModel { get; set; } = new CatalogViewModel();

		public async Task OnGet()
		{
			CatalogModel = await _catalogService.GetCatalogItemsAsync(CurrentPage, 10, CatalogModel.PlatformsFilterApplied, CatalogModel.TypesFilterApplied);
			Count = CatalogModel.TotalCount;
		}

		public IActionResult OnPostViewItem(int prodId)
		{
			//return RedirectToPage("CatalogItem", "LoadItem", new { prodId });
			return Redirect(string.Format("~/CatalogItem/LoadItem/{0}", prodId));
		}
	}
}
