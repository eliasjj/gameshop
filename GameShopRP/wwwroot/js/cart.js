﻿function incQuantity(id) {
    var c = parseInt(document.getElementById("inp" + id).value);
    document.getElementById("inp" + id).value = ++c;
}

function decQuantity(id) {
    var c = parseInt(document.getElementById("inp" + id).value);
    document.getElementById("inp" + id).value = --c;
}