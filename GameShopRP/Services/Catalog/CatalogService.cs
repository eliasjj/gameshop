﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameShopRP.Data.Repositories;
using GameShopRP.Models;
using GameShopRP.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GameShopRP.Services.Catalog
{
	public class CatalogService : ICatalogService
	{
		private readonly IAsyncRepository<Product> _productRepository;
		private readonly IAsyncRepository<Platform> _platformRepository;
		private readonly IAsyncRepository<Models.Type> _typeRepository;

		public CatalogService(IAsyncRepository<Product> productRepos,
			IAsyncRepository<Platform> platformRepos,
			IAsyncRepository<Models.Type> typeRepos)
		{
			_productRepository = productRepos;
			_platformRepository = platformRepos;
			_typeRepository = typeRepos;
		}

		public async Task<CatalogViewModel> GetCatalogItemsAsync(int currentPage, int pageSize, int? typeId, int? platformId)
		{
			var productsList = await _productRepository.ListAllAsync();
			var productsListOnPage = productsList.OrderBy(p => p.Id).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

			foreach (var product in productsListOnPage)
			{
				product.ImageUrl = "/img/productsCards/" + product.ImageUrl;
			}

			var catalogVm = new CatalogViewModel()
			{
				CatalogPageItems = productsListOnPage.Select(i => new CatalogCardViewModel()
				{
					Id = i.Id,
					Name = i.Name,
					ImageUrl = i.ImageUrl,
					Price = i.Price
				}),
				Platforms = await GetPlatformsAsync(),
				Types = await GetTypesAsync(),
				PlatformsFilterApplied = platformId ?? 0,
				TypesFilterApplied = typeId ?? 0,
				TotalCount = productsList.Count
			};

			return catalogVm;
		}

		public async Task<CatalogItemViewModel> GetCatalogItemByIdAsync(int productId)
		{
			var product = await _productRepository.GetByIdAsync(productId);
			product.ImageUrl = "/img/productsCovers/" + product.ImageUrl;
			var civm = new CatalogItemViewModel
			{
				Id = product.Id,
				Name = product.Name,
				Description = product.Description,
				Genre = product.Genre,
				Language = product.Language,
				ImageUrl = product.ImageUrl,
				Publisher = product.Publisher,
				Price = product.Price
			};

			return civm;
		}

		public async Task<IEnumerable<SelectListItem>> GetPlatformsAsync()
		{
			var platforms = await _platformRepository.ListAllAsync();
			var items = new List<SelectListItem>
			{
				new SelectListItem() { Value = null, Text = "All", Selected = true }
			};
			foreach (Platform platform in platforms)
			{
				items.Add(new SelectListItem() { Value = platform.Id.ToString(), Text = platform.Name });
			}

			return items;
		}

		public async Task<IEnumerable<SelectListItem>> GetTypesAsync()
		{
			var types = await _typeRepository.ListAllAsync();
			var items = new List<SelectListItem>
			{
				new SelectListItem() { Value = null, Text = "All", Selected = true }
			};
			foreach (Models.Type type in types)
			{
				items.Add(new SelectListItem() { Value = type.Id.ToString(), Text = type.Name });
			}

			return items;
		}
	}
}
