﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GameShopRP.Services.Order;
using GameShopRP.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GameShopRP.Pages
{
    public class OrderModel : PageModel
    {
		private readonly IOrderService _orderService;

		public OrderViewModel OrderViewModel { get; set; } = new OrderViewModel();

		public OrderModel(IOrderService orderService)
		{
			_orderService = orderService;
		}

        public async Task OnGet()
        {
			OrderViewModel = await _orderService.GetOrderItemsAsync(User.FindFirstValue(ClaimTypes.NameIdentifier));
        }

		public async Task<IActionResult> OnPostDeleteOrderLine(int orderLineId)
		{
			await _orderService.DeleteOrderLine(orderLineId);
			return RedirectToPage("Order");
		}

		public async Task<IActionResult> OnPostUpdateQuantity(int orderLineId, int quantity)
		{
			await _orderService.UpdateQuantity(orderLineId, quantity);
			return RedirectToPage("Order");
		}
    }
}