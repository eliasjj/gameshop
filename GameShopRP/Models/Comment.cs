﻿using GameShopRP.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.Models
{
	public class Comment
	{
		public int Id { get; set; }
		[Required]
		public string Text { get; set; }
		public DateTime Time { get; set; }

		public ShopUser ShopUser { get; set; }
	}
}
