﻿using GameShopRP.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.Data
{
	public class ShopContext : IdentityDbContext<ShopUser>
	{
		public ShopContext(DbContextOptions<ShopContext> options)
			: base(options)
		{
		}

		public DbSet<Comment> Comments { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<OrderLine> OrderLines { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Platform> Platforms { get; set; }
		public DbSet<Models.Type> Types { get; set; }

		//protected override void OnModelCreating(ModelBuilder builder)
		//{
		//	base.OnModelCreating(builder);
		//}
	}
}
