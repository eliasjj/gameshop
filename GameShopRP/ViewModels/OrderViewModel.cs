﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.ViewModels
{
	public class OrderViewModel
	{
		public List<OrderItemViewModel> OrderItems { get; set; } = new List<OrderItemViewModel>();
		public string UserId { get; set; }

		public decimal Total()
		{
			return Math.Round(OrderItems.Sum(x => x.ProductPrice * x.Quantity), 2);
		}
	}
}
