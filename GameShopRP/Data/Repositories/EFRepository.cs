﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GameShopRP.Data.Repositories
{
	public class EfRepository<T> : IAsyncRepository<T> where T : class 
	{
		protected readonly ShopContext _dbContext;
		DbSet<T> _dbSet;

		public EfRepository(ShopContext dbContext)
		{
			_dbContext = dbContext;
			_dbSet = dbContext.Set<T>();
		}

		public virtual async Task<T> GetByIdAsync(int id)
		{
			return await _dbSet.FindAsync(id);
		}

		public async Task<List<T>> ListAllAsync()
		{
			return await _dbSet.AsNoTracking().ToListAsync();
			//return await _dbContext.Set<T>().ToListAsync();
		}

		public async Task<List<T>> GetListAsync(Expression<Func<T, bool>> expression)
		{
			return await _dbSet.AsNoTracking().Where(expression).ToListAsync();
		}

		public async Task<List<T>> GetListAsync(string navigationPropertyPath)
		{
			return await _dbSet.AsNoTracking().Include(navigationPropertyPath).ToListAsync();
		}

		public async Task<List<T>> GetListAsync(Expression<Func<T, bool>> expression, string navigationPropertyPath)
		{
			return await _dbSet.AsNoTracking().Where(expression).Include(navigationPropertyPath).ToListAsync();
		}

		public async Task<List<T>> GetListAsync(Expression<Func<T, object>> expression)
		{
			return await _dbSet.AsNoTracking().Include(expression).ToListAsync();
		}

		public async Task<T> AddAsync(T entity)
		{
			_dbSet.Add(entity);
			await _dbContext.SaveChangesAsync();

			return entity;
		}

		public async Task UpdateAsync(T entity)
		{
			_dbContext.Entry(entity).State = EntityState.Modified;
			await _dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(T entity)
		{
			_dbSet.Remove(entity);
			await _dbContext.SaveChangesAsync();
		}
	}
}
