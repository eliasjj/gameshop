﻿using GameShopRP.Data.Repositories;
using GameShopRP.Models;
using GameShopRP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.Services.Order
{
	public class OrderService : IOrderService
	{
		private readonly IAsyncRepository<Models.Order> _ordersRepository;
		private readonly IAsyncRepository<OrderLine> _orderLinesRepository;

		public OrderService(IAsyncRepository<Models.Order> ordersRepository,
			IAsyncRepository<OrderLine> orderLinesRepository)
		{
			_ordersRepository = ordersRepository;
			_orderLinesRepository = orderLinesRepository;
		}

		public async Task AddItemToOrder(string userId, int productId, int quantity = 1)
		{
			var order = await GetOrCreateOrder(userId);
			if (!order.OrderLines.Any(i => i.ProductId == productId))
			{
				await _orderLinesRepository.AddAsync(new OrderLine()
				{
					OrderId = order.Id,
					ProductId = productId,
					Quantity = quantity
				});
			}
			else
			{
				var existingItem = order.OrderLines.FirstOrDefault(i => i.ProductId == productId);
				existingItem.Quantity += quantity;
				await _orderLinesRepository.UpdateAsync(existingItem);
			}
			// todo order item adding 
		}

		private async Task<Models.Order> GetOrCreateOrder(string userId)
		{
			var orders = await _ordersRepository.GetListAsync(o => o.ShopUserId == userId && o.IsCheckedOut == false, "OrderLines"); // replace with lambda
			if (orders.Count == 0)
				return await CreateOrderByUserId(userId);
			return orders.Single();
		}

		private async Task<Models.Order> CreateOrderByUserId(string userId)
		{
			Models.Order order = new Models.Order() { ShopUserId = userId, IsCheckedOut = false, OrderLines = new List<OrderLine>() };
			await _ordersRepository.AddAsync(order);

			return order;
		}

		public async Task<OrderViewModel> GetOrderItemsAsync(string userId)
		{
			var orders = await _ordersRepository.GetListAsync(o => o.ShopUserId == userId && o.IsCheckedOut == false, "OrderLines"); // replace with lambda
			if (orders.Count == 0)
			{
				var order = CreateOrderByUserId(userId);
				return new OrderViewModel()
				{
					OrderItems = new List<OrderItemViewModel>()
				};
			}
			else
			{
				var orderLines = await _orderLinesRepository.GetListAsync(ol => ol.OrderId == orders.Single().Id, "Product"); // replace with lambda
				return new OrderViewModel()
				{
					OrderItems = orderLines.Select(o => new OrderItemViewModel()
					{
						Id = o.Id,
						ProductId = o.ProductId,
						Quantity = o.Quantity,
						ImageUrl = ("/img/productsCovers/" + o.Product.ImageUrl),
						ProductName = o.Product.Name,
						ProductPrice = o.Product.Price
					}).ToList(),
					UserId = userId
				};
			}
		}

		public async Task DeleteOrderLine(int orderLineId)
		{
			var orderLine = await _orderLinesRepository.GetByIdAsync(orderLineId);
			if (orderLine != null)
			{
				await _orderLinesRepository.DeleteAsync(orderLine);
			}
		}

		public async Task UpdateQuantity(int orderLineId, int quantity)
		{
			var orderLine = await _orderLinesRepository.GetByIdAsync(orderLineId);
			if (orderLine != null)
			{
				orderLine.Quantity = quantity;
				await _orderLinesRepository.UpdateAsync(orderLine);
			}
		}
	}
}
