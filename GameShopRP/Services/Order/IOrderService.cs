﻿using GameShopRP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.Services.Order
{
	public interface IOrderService
	{
		Task AddItemToOrder(string userId, int productId, int quantity = 1);
		Task<OrderViewModel> GetOrderItemsAsync(string userId);
		Task DeleteOrderLine(int orderLineId);
		Task UpdateQuantity(int orderLineId, int quantity);
	}
}
