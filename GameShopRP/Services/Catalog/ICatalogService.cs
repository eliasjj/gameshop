﻿using GameShopRP.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.Services.Catalog
{
	public interface ICatalogService
	{
		Task<CatalogViewModel> GetCatalogItemsAsync(int pageIndex, int itemsOnPage, int? typeId, int? platformID);
		Task<CatalogItemViewModel> GetCatalogItemByIdAsync(int productId);
		Task<IEnumerable<SelectListItem>> GetTypesAsync();
		Task<IEnumerable<SelectListItem>> GetPlatformsAsync();
	}
}
