﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameShopRP.ViewModels
{
	public class CatalogViewModel
	{
		public IEnumerable<CatalogCardViewModel> CatalogPageItems { get; set; }
		public IEnumerable<SelectListItem> Platforms { get; set; }
		public IEnumerable<SelectListItem> Types { get; set; }
		public int? PlatformsFilterApplied { get; set; }
		public int? TypesFilterApplied { get; set; }
		public int TotalCount { get; set; }
	}
}
